<?php
/**
 * GRAnet Wordpress extensions for the Gerrit Rietveld Academie intranet
 * (cc) 2013
 *
 * @author Luis Rodil-Fernandez <dropmeaword@gmail.com>
 */

add_theme_support( 'menus' );

/** register menu locations */
function register_my_menus() {
	register_nav_menus(
		array( 
			'intra-footer-menu' => __( 'Intranet Footer Menu' ),
			'intra-nav-menu' => __( 'Intranet Navigation Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

// @lf

/** register sidebar for member content only */ 
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
	  'name' => __( 'Internal Only sidebar' ),
	  'id' => 'internal-only-sidebar',
	  'description' => __( 'Widgets in this area will be shown to members of staff only.' ),
	  'before_title' => '<h1>',
	  'after_title' => '</h1>'
	));
}

/** add post-type for internal documents */
if ( function_exists('register_post_type') ) {
	register_post_type('intranet', array(
			'label' => 'Intranet',
			'public' => true,
			'show_ui' => true,
			'capability_type' => 'page',
			'hierarchical' => true,
			'rewrite' => array('slug' => 'intranet'),
			'query_var' => true,
			'supports' => array(
										'title',
										'editor',
										'custom-fields',
										'revisions',
										'thumbnail',
										'author',
										'page-attributes',
									)
			) 
	);
	// error_log("registering custom post type");
}

/** add taxonomy for public/private content */
add_action( 'init', 'build_taxonomies', 0 ); 
function build_taxonomies() {
	register_taxonomy( 'content_domain', 
										 'intranet', array( 'hierarchical' => true, 
																		'label' => 'Content Domains', 
																		'query_var' => true, 
																		'rewrite' => true 
																	)
										);

	register_taxonomy_for_object_type( 'content_domain', 'intranet' );
}

?>