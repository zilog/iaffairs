<?php

/********************************************************

* Do not modify unless you know what you're doing, SERIOUSLY!

********************************************************/



$themename = "Inuit Types";

$shortname = "inuitypes";

$template_path = get_bloginfo('template_directory');



$alt_stylesheet_path = TEMPLATEPATH . '/skins/';

$alt_stylesheets = array();

$frame_path = TEMPLATEPATH . '/frames/'; 

$frames = array();

$pn_categories_obj = get_categories('hide_empty=0');

$pn_categories = array();

$pne_categories_obj = get_categories('hide_empty=0');

$pne_categories = array();

$pne_pages_obj = get_pages('sort_order=ASC');

$pne_pages = array();



// Alternative Stylesheet Load

if ( is_dir($alt_stylesheet_path) ) {

	if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 

		while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {

			if(stristr($alt_stylesheet_file, ".css") !== false) {

				$alt_stylesheets[] = $alt_stylesheet_file;

			}

		}	

	}

}

	

// Categories Name Load

foreach ($pn_categories_obj as $pn_cat) {

	$pn_categories[$pn_cat->cat_ID] = $pn_cat->cat_name;

}

$categories_tmp = array_unshift($pn_categories, "Select a category:");



// Pages Exclude Load

foreach ($pne_pages_obj as $pne_pag) {

	$pne_pages[$pne_pag->ID] = $pne_pag->post_title;

}



// Exclude Pages by Name

function pages_exclude($options) {

	$options[] = array(	"type" => "wraptop");						

	$pags = get_pages('sort_order=ASC');	

	foreach ($pags as $pag) {	

			$options[] = array(	"name" => "",

						"desc" => "",

						"label" => $pag->post_title,

						"id" => "pag_exclude_".$pag->ID,

						"std" => "",

						"type" => "checkbox");					

	}	

	$options[] = array(	"type" => "wrapbottom");		

	return $options;

}



// Custom Page List

function get_inc_pages($label) {	

	$include = '';

	$counter = 0;

	$pagsx = get_pages('sort_order=ASC');	

	foreach ($pagsx as $pag) {		

		$counter++;		

		if ( get_option( $label.$pag->ID ) ) {

			if ( $counter <> 1 ) { $include .= ','; }

			$include .= $pag->ID;

			}	

	}	

	return $include;

}



$other_entries = array("Select a Number:","0","1","2","3","4","5","6","7","8","9","10");

$feed_server = array("","http://feeds.feedburner.com/","http://feeds2.feedburner.com/","http://feedproxy.google.com/");





				$options[] = array(	"type" => "maintabletop");

////// General Settings

				$options[] = array(	"name" => "General Settings",

						"type" => "heading");

						

				$options[] = array(	"name" => "Theme skin",

						"desc" => "Please select the CSS skin of your blog here.",

					    "id" => $shortname."_alt_stylesheet",

					    "std" => "Select a CSS skin:",

					    "type" => "select",

					    "options" => $alt_stylesheets);

				

				$options[] = array(	"name" => "Administration Link",

					    "label" => "Show administration link on frontpage.",

						"desc" => "Show admin link in top right menu, near RSS links.",

					    "id" => $shortname."_admin_link",

					    "std" => "true",

					    "type" => "checkbox");



				$options[] = array(	"name" => "Sidebar on the left or right?",

					    "label" => "Show sidebar content on the right.",

					    "id" => $shortname."_right_sidebar",

					    "std" => "true",

					    "type" => "checkbox");		

				

				$options[] = array(	"name" => "Relative dates in posts",

						"label" => "Show relative dates in posts.",

						"desc" => "i.e. 'Posted 2 weeks, 1 day ago'",

						"id" => $shortname."_relative_date",

						"std" => "false",

						"type" => "checkbox");	



				$options[] = array(	"name" => "Favicon",

						"desc" => "Paste the full URL for your favicon image here if you wish to show it in browsers. Create one <a href='http://www.favicon.cc/'>here</a>",

						"id" => $shortname."_favicon",

						"std" => $template_path . "/images/favicon.ico",

						"type" => "text");					



                $options[] = array(	"name" => "Logo",

						"desc" => "Paste the full URL to your logo here.",

						"id" => $shortname."_logo_url",

						"std" => $template_path . "/images/logo-trans.png",

						"type" => "text");



				$options[] = array(	"name" => "Choose Blog Title over Logo",

						"label" => "Show text title instead of Image logotype.",

						"id" => $shortname."_show_blog_title",

						"std" => "false",

						"type" => "checkbox");			



				$options[] = array( "name" => "Feedburner Setup",

						"desc" => "Choose your Feedburner feed server (<em>leave empty if you want to use default WordPress feed engine</em>). ",

			    		"id" => $shortname."_feedburner_address",

			    		"std" => "Choose Your Feedburner Server Address",

			    		"type" => "select",

			    		"options" => $feed_server);

						

				$options[] = array(

						"desc" => "Enter your Feedburner Feed Name here. <br/>(i.e. 'MYFEED' from 'http://feeds.feedburner.com/MYFEED')",

			    		"id" => $shortname."_feedburner_feedname",

			    		"std" => "",

			    		"type" => "text");	

						

				$options[] = array(	"name" => "Header Scripts",

					    "desc" => "If you need to add scripts to your header (like <a href='http://haveamint.com/'>Mint</a> tracking code), do so here.",

					    "id" => $shortname."_scripts_header",

					    "std" => "",

					    "type" => "textarea");

						

				$options[] = array(	"name" => "Footer Scripts",

					    "desc" => "If you need to add scripts to your footer (like <a href='http://www.google.com/analytics/'>Google Analytics</a> tracking code), do so here.",

					    "id" => $shortname."_google_analytics",

					    "std" => "",

					    "type" => "textarea");

				

				$options[] = array(	"name" => "Exclude Pages from Header Menu",

						"type" => "multihead");

						

				$options = pages_exclude($options);

				

				$options[] = array(	"type" => "maintablebreak");

				

/// Home Page Settings												

				$options[] = array(	"name" => "Home Page Settings",

						"type" => "heading");

				

				$options[] = array(	"name" => "About Your Blog",

					    "desc" => "Write something about your blog here. Choose your words wisely and write only what is relevant to your blog content! --> This section will be visible on top of your front page.",

					    "id" => $shortname."_about_blog",

					    "std" => "",

					    "type" => "textarea2");

						

				$options[] = array(	"name" => "Featured Post Entries",

						"desc" => "Select max number of featured entries you wish to appear on homepage. Featured entries are the latest highlighted posts.",

			    		"id" => $shortname."_featured_entries",

			    		"std" => "Select a Number:",

			    		"type" => "select",

			    		"options" => $other_entries);

						

				$options[] = array(	"name" => "I Want One Column Featured Posts",

					"desc" => "Show featured posts in one column instead of default two columns",

					"id" => $shortname."_one_column_featposts",

					"std" => "false",

					"type" => "checkbox");



				$options[] = array(	"name" => "I Want One Column Normal Posts",

					"desc" => "Show normal posts in one column instead of default two columns",

					"id" => $shortname."_one_column_posts",

					"std" => "false",

					"type" => "checkbox");	

				

				$options[] = array(	"type" => "maintablebreak");

				

				$options[] = array(	"type" => "maintablebottom");

				

				$options[] = array(	"type" => "maintabletop");

////// Advertising scripts

                $options[] = array(	"name" => "Advertising Scripts",

						"type" => "heading");



				$options[] = array(	"name" => "Above Header",

					    "desc" => "Enter your ad script code here. Must be up to <b>728 width</b>.",

					    "id" => $shortname."_header_adsense",

					    "std" => "",

					    "type" => "textarea");

						

				$options[] = array(	"name" => "Below Header",

					    "desc" => "Enter your ad script code here. Must be up to <b>468 width</b>.",

					    "id" => $shortname."_main_adsense",

					    "std" => "",

					    "type" => "textarea");

				

				$options[] = array(	"name" => "Above Comments",

					    "desc" => "Enter your ad script code here. Must be up to <b>468 width</b>.",

					    "id" => $shortname."_comment_adsense",

					    "std" => "",

					    "type" => "textarea");

/// 300x250				

		        $options[] = array(	"type" => "maintablebreak");

				

				$options[] = array(	"name" => "Banner Ad 250x250 <br/>- <em>enable in Widgets</em>",

						"type" => "heading");



				$options[] = array(	"name" => "Disable 250x250 Ad Banner",

						"label" => "Ignore the 250x250 Ad.",

						"id" => $shortname."_not_200",

						"std" => "true",

						"type" => "checkbox");



				$options[] = array(	"name" => "Display Only On Homepage?",

						"label" => "Display this ad only on homepage. ",

						"id" => $shortname."_home_only",

						"std" => "false",

						"type" => "checkbox");



				$options[] = array(	"name" => "250x250 Ad - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_block_image",

						"std" => $template_path . "/images/250x250.png",

						"type" => "text");

						

				$options[] = array(	"name" => "250x250 Ad - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_block_url",

						"std" => "http://wordpress.org/extend/themes/download/inuitypes.zip",

			    		"type" => "text");

/// 125x125 top

				$options[] = array(	"type" => "maintablebreak");

				

				$options[] = array(	"name" => "Banner Ads 125x125 <br/>- <em>enable in Widgets</em>",

						"type" => "heading");



				$options[] = array(	"name" => "Display two (1 and 2) 125x125 ads",

						"label" => "Display top 1st and 2nd Ad in sidebar.",

						"id" => $shortname."_show_ads_top12",

						"std" => "false",

						"type" => "checkbox");



				$options[] = array(	"name" => "#1 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_1",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#1 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_1",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");						



				$options[] = array(	"name" => "#2 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_2",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#2 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_2",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");



				$options[] = array(	"name" => "Display two (3 and 4) 125x125 ads",

						"label" => "Display 2nd and 3rd Ad in sidebar.",

						"id" => $shortname."_show_ads_top34",

						"std" => "false",

						"type" => "checkbox");



				$options[] = array(	"name" => "#3 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_3",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#3 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_3",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");						



				$options[] = array(	"name" => "#4 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_4",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#4 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_4",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");

						

				$options[] = array(	"name" => "Display two (5 and 6) 125x125 ads",

						"label" => "Display top 5th and 6th Ad in sidebar.",

						"id" => $shortname."_show_ads_top56",

						"std" => "false",

						"type" => "checkbox");

						

				$options[] = array(	"name" => "#5 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_5",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#5 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_5",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");

						

				$options[] = array(	"name" => "#6 - Image Location",

						"desc" => "Enter the URL for this Ad.",

						"id" => $shortname."_ad_image_6",

						"std" => $template_path . "/images/ad-125x125.png",

						"type" => "text");

						

				$options[] = array(	"name" => "#6 - Destination",

						"desc" => "Enter the URL where this Ad points to.",

			    		"id" => $shortname."_ad_url_6",

						"std" => "http://www.bizzartic.com",

			    		"type" => "text");

						

				$options[] = array(	"type" => "maintablebreak");

												

				$options[] = array(	"type" => "maintablebottom");

				

				$options[] = array(	"type" => "maintabletop");



//////Translations				

				$options[] = array(	"name" => "Translations",

						"type" => "heading");

				

				$options[] = array(	"name" => "Administration Link Text",

						"desc" => "Change Admin link text",

			    		"id" => $shortname."_admin_name",

			    		"std" => "Admin",

			    		"type" => "text");

				

				$options[] = array(	"name" => "Subscribe Text",

						"desc" => "Change Subscribe text",

			    		"id" => $shortname."_subscribe_name",

			    		"std" => "Subscribe",

			    		"type" => "text");

						

				$options[] = array(	"name" => "Sitemap Text",

						"desc" => "Change Pages text",

			    		"id" => $shortname."_pages_name",

			    		"std" => "Pages",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change Last 60 blog posts text",

			    		"id" => $shortname."_last_posts",

			    		"std" => "Last 60 Blog Posts",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change Monthly archives text",

			    		"id" => $shortname."_monthly_archives",

			    		"std" => "Monthly Archives",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change Categories text",

			    		"id" => $shortname."_categories_name",

			    		"std" => "Categories",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change RSS feeds text",

			    		"id" => $shortname."_rssfeeds_name",

			    		"std" => "Available RSS Feeds",

			    		"type" => "text");

						

				$options[] = array(	"name" => "404 Error Text",

						"desc" => "Change 404 error text",

			    		"id" => $shortname."_404error_name",

			    		"std" => "Error 404 | Page not found!",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change 404 error solution text",

			    		"id" => $shortname."_404solution_name",

			    		"std" => "Sorry, but you are looking for something that is not here.",

			    		"type" => "text");

						

				$options[] = array(	"name" => "Archive Text",

						"desc" => "Change browsing category text",

			    		"id" => $shortname."_browsing_category_name",

			    		"std" => "",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change browsing day/month text",

			    		"id" => $shortname."_browsing_daymonth_name",

			    		"std" => "x",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change browsing year text",

			    		"id" => $shortname."_browsing_year_name",

			    		"std" => "-",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change search results text",

			    		"id" => $shortname."_browsing_search_name",

			    		"std" => "Search Results for",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change no search results text",

			    		"id" => $shortname."_browsing_searchresults_name",

			    		"std" => "There are NO Search Results for",

			    		"type" => "text");

						

				$options[] = array(	

						"desc" => "Change author archive text",

			    		"id" => $shortname."_browsing_author_name",

			    		"std" => "Browsing Archives of Author",

			    		"type" => "text");

						

				$options[] = array(	"name" => "Comments Text",

						"desc" => "Change password protected text",

			    		"id" => $shortname."_password_protected_name",

			    		"std" => "This post is password protected. Enter the password to view comments.",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change no responses text",

			    		"id" => $shortname."_comment_responsesa_name",

			    		"std" => "No Responses",

			    		"type" => "text");

				

				$options[] = array(

						"desc" => "Change one response text",

			    		"id" => $shortname."_comment_responsesb_name",

			    		"std" => "One Response",

			    		"type" => "text");

				

				$options[] = array(

						"desc" => "Change multiple responses text, leave % intact!",

			    		"id" => $shortname."_comment_responsesc_name",

			    		"std" => "% Responses",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change trackbacks text",

			    		"id" => $shortname."_comment_trackbacks_name",

			    		"std" => "Trackbacks For This Post",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change comment moderation text",

			    		"id" => $shortname."_comment_moderation_name",

			    		"std" => "Your comment is awaiting moderation.",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change start conversation text",

			    		"id" => $shortname."_comment_conversation_name",

			    		"std" => "Ben de eerste",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change closed comments text",

			    		"id" => $shortname."_comment_closed_name",

			    		"std" => "Comments are closed.",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change leave a reply text",

			    		"id" => $shortname."_comment_reply_name",

			    		"std" => "Leave a Reply",

			    		"type" => "text");

				

				$options[] = array(

						"desc" => "Change 'you must be' text",

			    		"id" => $shortname."_comment_mustbe_name",

			    		"std" => "You must be",

			    		"type" => "text");

				

				$options[] = array(

						"desc" => "Change 'logged in' text",

			    		"id" => $shortname."_comment_loggedin_name",

			    		"std" => "logged in",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change 'to post a comment' text",

			    		"id" => $shortname."_comment_postcomment_name",

			    		"std" => "to post a comment.",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change Logout text",

			    		"id" => $shortname."_comment_logout_name",

			    		"std" => "Logout",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change name text",

			    		"id" => $shortname."_comment_name_name",

			    		"std" => "Name",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change mail text",

			    		"id" => $shortname."_comment_mail_name",

			    		"std" => "Mail",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change website text",

			    		"id" => $shortname."_comment_website_name",

			    		"std" => "Website",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change add comment text",

			    		"id" => $shortname."_comment_addcomment_name",

			    		"std" => "Add Comment",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change 'reply' to threaded comment text",

			    		"id" => $shortname."_comment_justreply_name",

			    		"std" => "Reply",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change 'edit' comment text, only visible to administrators",

			    		"id" => $shortname."_comment_edit_name",

			    		"std" => "Edit",

			    		"type" => "text");

						

				$options[] = array(	"name" => "Pagination Text",

						"desc" => "Change first page text",

			    		"id" => $shortname."_pagination_first_name",

			    		"std" => "First",

			    		"type" => "text");

						

				$options[] = array(

						"desc" => "Change last page text",

			    		"id" => $shortname."_pagination_last_name",

			    		"std" => "Last",

			    		"type" => "text");

						

				$options[] = array(	"type" => "maintablebreak");

						

				$options[] = array(	"type" => "maintablebottom");

																														

		

function mytheme_add_admin() {



    global $themename, $shortname, $options;



    if ( $_GET['page'] == basename(__FILE__) ) {

    

        if ( 'save' == $_REQUEST['action'] ) {

	

                foreach ($options as $value) {

					if($value['type'] != 'multicheck'){

                    	update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 

					}else{

						foreach($value['options'] as $mc_key => $mc_value){

							$up_opt = $value['id'].'_'.$mc_key;

							update_option($up_opt, $_REQUEST[$up_opt] );

						}

					}

				}



                foreach ($options as $value) {

					if($value['type'] != 'multicheck'){

                    	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } 

					}else{

						foreach($value['options'] as $mc_key => $mc_value){

							$up_opt = $value['id'].'_'.$mc_key;						

							if( isset( $_REQUEST[ $up_opt ] ) ) { update_option( $up_opt, $_REQUEST[ $up_opt ]  ); } else { delete_option( $up_opt ); } 

						}

					}

				}

                header("Location: themes.php?page=functions.php&saved=true");

                die;



        } else if( 'reset' == $_REQUEST['action'] ) {



            foreach ($options as $value) {

				if($value['type'] != 'multicheck'){

                	delete_option( $value['id'] ); 

				}else{

					foreach($value['options'] as $mc_key => $mc_value){

						$del_opt = $value['id'].'_'.$mc_key;

						delete_option($del_opt);

					}

				}

			}

            header("Location: themes.php?page=functions.php&reset=true");

            die;



        }

    }



    add_theme_page($themename." Options", "$themename Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');



}



function mytheme_admin() {



    global $themename, $shortname, $options;



    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';

    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';

    

?>

<div class="wrap">

<h2><?php echo $themename; ?> Theme Settings</h2>



        <div class="introduction">

			<p>Check <a href="<?php echo get_bloginfo('template_directory'); ?>/support_guide.txt" onclick="window.open(this.href, 'popupwindow', 'width=700,height=600,scrollbars,resizable'); return false;">theme support guide</a> before you start

			... Don't hesitate to submit <a href="http://www.kampyle.com/feedback_form/ff-feedback-form.php?site_code=9849398&amp;form_id=12315&amp;lang=en" onclick="window.open(this.href, 'popupwindow', 'width=438,height=500,scrollbars,resizable'); return false;">Questions, Bug Reports, Compliments, Copyright Issues</a> to theme author!</p>

			<p>

<!-- ++Begin Dynamic Feed Wizard Generated Code++ -->

  <!--

  // Created with a Google AJAX Search and Feed Wizard

  // http://code.google.com/apis/ajaxsearch/wizards.html

  -->



  <!--

  // The Following div element will end up holding the actual feed control.

  // You can place this anywhere on your page.

  -->

  <div id="feed-control">

    <span style="color:#676767;font-size:11px;margin:10px;padding:4px;">Loading...</span>

  </div>



  <!-- Google Ajax Api

  -->

  <script src="http://www.google.com/jsapi?key=notsupplied-wizard"

    type="text/javascript"></script>



  <!-- Dynamic Feed Control and Stylesheet -->

  <script src="http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.js"

    type="text/javascript"></script>

  <style type="text/css">

    @import url("http://www.google.com/uds/solutions/dynamicfeed/gfdynamicfeedcontrol.css");

  </style>

  <style>

	.fl { float: left }

	.fr { float: right }

	.clear { clear: both }

	.gfg-horizontal-root { background: #fff; margin: 0 0 10px 0 }

	.gfg-branding { visibility: hidden }

	 h2 { margin-bottom: 20px }

    .introduction { background: #transparent; padding: 0 10px; color: #333; margin: 15px 52px 15px 0; }

	

	.submit-title { margin: -38px 0 0 0; padding:0 10px 0 0 }

    .box-title { margin: 0 0 5px 0 !important; background: #ccc; padding: 10px 1em 10px 1em; font-family: Georgia, serif; font-weight: normal !important; letter-spacing: 1px; font-size: 17px; }

    .maintable { font-family:"Lucida Grande","Lucida Sans Unicode",Arial,Verdana,sans-serif; background: #eee; padding: 0; border: 0; width: 325px; margin-right: 20px; float: left; }

	.table-row { color: #000; margin: 0; padding: 0 1em; }

	.left-container { color: #444; font: bold 13px "Helvetica Neue", Helvetica, Arial, sans-serif; border: none; margin: 0; padding: 0 }

    .right-container { border: none; margin: 0; padding: 0 0 10px 0; border-bottom: 1px dashed #BFBFBF; }

	.right-container .description { margin: -25px 0 0 0; font: normal 11px "Helvetica Neue", Helvetica, Arial, sans-serif; margin-bottom: 0; color: #828282; }

	.space-line { clear: both; margin: 0; padding: 0; width: auto }

	.wrap-dropdown { text-align:left; width: 22em; height: 80px; overflow: auto; border: 1px solid #7F9DB9; background: #fff }

	.wrap-dropdown .left-container { width: 0% }

	.wrap-dropdown .right-container { width: 100%; border: 0; padding: 0 }

	.wrap-dropdown .right-container .text { margin: 4px 0 5px 5px }

	.maintable .table-row input, .maintable .table-row textarea { font: normal 0.923em/1.667em Consolas, "Andale Mono", "Courier New", Courier; width: 26em; padding: 0.333em; color: #3C3D35; background: #fff; border: 0.083em solid #BFBFBF; border-top-color: #A5A5A5; border-left-color: #A5A5A5; }

	.maintable .table-row input:focus, .maintable .table-row textarea:focus { background: #DDE4DE; }

	.maintable .table-row select { font: normal 1em/1.538em "Lucida Grande", "Lucida Sans Unicode", Tahoma, Verdana, sans-serif; width: 16.154em; padding: 0.308em; }

    .maintable .table-row input.input_checkbox { width: 20px; border: 0; background: transparent; }

	.submit-title input { width: 95px }

  </style>



  <script type="text/javascript">

    function LoadDynamicFeedControl() {

      var feeds = [

	{title: 'BizzArtic - Ideas for Blogging',

	 url: 'http://bizzartic.com/feed/'

	},

	{title: 'ProBlogger - Tips for Blogging',

	 url: 'http://feeds.feedburner.com/ProbloggerHelpingBloggersEarnMoney'

	},

	{title: 'Daily Blog Tips',

	 url: 'http://feeds.feedburner.com/DailyBlogTips'

	}];

      var options = {

        stacked : false,

        horizontal : true,

        title : ""

      }



      new GFdynamicFeedControl(feeds, 'feed-control', options);

    }

    // Load the feeds API and set the onload callback.

    google.load('feeds', '1');

    google.setOnLoadCallback(LoadDynamicFeedControl);

  </script>

<!-- ++End Dynamic Feed Control Wizard Generated Code++ -->

			</p>

		</div>



<form method="post">



<?php foreach ($options as $value) { 

	

	switch ( $value['type'] ) {

		case 'text':

		option_wrapper_header($value);

		?>

		        <input class="text_input" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" />

		<?php

		option_wrapper_footer($value);

		break;

		

		case 'select':

		option_wrapper_header($value);

		?>

	            <select class="select_input" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">

	                <?php foreach ($value['options'] as $option) { ?>

	                <option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>

	                <?php } ?>

	            </select>

		<?php

		option_wrapper_footer($value);

		break;

		

		case 'textarea':

		$ta_options = $value['options'];

		option_wrapper_header($value);

		?>

				<textarea name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" cols="<?php echo $ta_options['cols']; ?>" rows="8"><?php  if( get_settings($value['id']) != "") { echo stripslashes(get_settings($value['id'])); } else { echo $value['std']; } ?></textarea>

		<?php

		option_wrapper_footer($value);

		break;

		

		case 'textarea2':

		$ta_options = $value['options'];

		option_wrapper_header($value);

		?>

				<textarea name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" cols="<?php echo $ta_options['cols']; ?>" rows="2"><?php  if( get_settings($value['id']) != "") { echo stripslashes(get_settings($value['id'])); } else { echo $value['std']; } ?></textarea>

		<?php

		option_wrapper_footer($value);

		break;



		case "radio":

		option_wrapper_header($value);

		

 		foreach ($value['options'] as $key=>$option) { 

				$radio_setting = get_settings($value['id']);

				if($radio_setting != ''){

		    		if ($key == get_settings($value['id']) ) {

						$checked = "checked=\"checked\"";

						} else {

							$checked = "";

						}

				}else{

					if($key == $value['std']){

						$checked = "checked=\"checked\"";

					}else{

						$checked = "";

					}

				}?>

	            <input type="radio" name="<?php echo $value['id']; ?>" value="<?php echo $key; ?>" <?php echo $checked; ?> /><?php echo $option; ?><br />

		<?php 

		}

		 

		option_wrapper_footer($value);

		break;

		

		case "checkbox":

		option_wrapper_header($value);

						if(get_settings($value['id'])){

							$checked = "checked=\"checked\"";

						}else{

							$checked = "";

						}

					?>

		            <input class="input_checkbox" type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />&nbsp;<label><?php echo $value['label']; ?></label><br />

		<?php

		option_wrapper_footer($value);

		break;

		

		case "multicheck":

		option_wrapper_header($value);

		

 		foreach ($value['options'] as $key=>$option) {

	 			$pn_key = $value['id'] . '_' . $key;

				$checkbox_setting = get_settings($pn_key);

				if($checkbox_setting != ''){

		    		if (get_settings($pn_key) ) {

						$checked = "checked=\"checked\"";

						} else {

							$checked = "";

						}

				}else{

					if($key == $value['std']){

						$checked = "checked=\"checked\"";

					}else{

						$checked = "";

					}

				}?>

	            <input type="checkbox" name="<?php echo $pn_key; ?>" id="<?php echo $pn_key; ?>" value="true" <?php echo $checked; ?> /><label for="<?php echo $pn_key; ?>"><?php echo $option; ?></label><br />

		<?php 

		}

		 

		option_wrapper_footer($value);

		break;

		

		case "heading":

		?>

		

		    <div class="box-title"><?php echo $value['name']; ?>

			</div>

			<div class="fr submit submit-title">

                <input name="save" type="submit" value="Save changes" />    

                <input type="hidden" name="action" value="save" />

            </div>

		

		<?php

		break;

		

		case "wraptop":

		?><div class="right-container"><p class="text"><div class="wrap-dropdown"><?php

		break;

		case "wrapbottom":

		?></div></p></div></div><div class="clear"></div><?php

		break;

		case "multihead":

		option_wrapper_header2($value);

		break;

		

		case "maintabletop":

		?><div class="maintable"><?php

		break;

		case "maintablebottom":

		?></div><?php

		break;

		case "maintablebreak":

		?><br/><?php

		break;

		

		default:



		break;

	}

}

?>



<div class="clear"></div>

<p class="submit">

<input name="save" type="submit" value="Save changes" />    

<input type="hidden" name="action" value="save" />

</p>

</form>

<form method="post">

<p class="submit">

<input name="reset" type="submit" value="Reset" />

<input type="hidden" name="action" value="reset" />

</p>

</form>



<?php

}



function option_wrapper_header2($values){

	?>

	<div class="table-row"> 

	    <div class="left-container"><p class="text"><?php echo $values['name']; ?></p></div>

	<?php

}



function option_wrapper_header($values){

	?>

	<div class="table-row"> 

	    <div class="left-container"><p class="text"><?php echo $values['name']; ?></p></div>

	    <div class="right-container"><p class="text">

	<?php

}



function option_wrapper_footer($values){

	?>

	    </p><br/>

		<div class="description"><?php echo $values['desc']; ?></div>

		</div>

	</div>

	<div class="space-line"></div>

	<?php 

}



function mytheme_wp_head() { 

	$stylesheet = get_option('inuitypes_alt_stylesheet');

	if($stylesheet != ''){?>

		<link href="<?php bloginfo('template_directory'); ?>/skins/<?php echo $stylesheet; ?>" rel="stylesheet" type="text/css" />

<?php }

} 





add_action('wp_head', 'mytheme_wp_head');

add_action('admin_menu', 'mytheme_add_admin'); 



if ( function_exists('register_sidebar') ) {

    register_sidebars(2,array(
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div><!--/widget-->',
        'before_title' => '<h3 class="hl">',
        'after_title' => '</h3>',
    ));
}

// @lf Added GRA specific functionality
require_once ($includes_path . 'granet/wp-granet.php');
	
// Widgets
require_once ($includes_path . 'library/functions/widgets_functions.php');

// Custom
require_once ($includes_path . 'library/functions/custom_functions.php');

// Comments
require_once ($includes_path . 'library/functions/comments_functions.php');

?>