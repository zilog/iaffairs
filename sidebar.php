<div class="sidebar <?php if ( !get_option('inuitypes_right_sidebar') ) { echo 'sidebar_left'; } else { echo 'sidebar_right'; } ?>">
    
    	<?php  if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
		
		<!-- THIS IS JUST A SPLASH SIDEBAR PAGE SO YOU DON'T NEED TO EDIT THIS, IT'S JUST FOR COSMETIC VIEW IN WORDPRESS THEME DIRECTORY PREVIEW -->

<div class="widget">
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
        <div>
        <input type="text" value="" name="s" id="s" />

	    <input type="submit" id="searchsubmit" value="Search" />
        </div>
    </form>
</div>

<div class="fix"></div>

<div class="widget">
        <h2 class="widget_title">Archives</h2>
			<ul>
				<?php wp_get_archives('type=monthly'); ?>
			</ul>
</div>

<div class="fix"></div>

<div class="widget">
        <h2 class="widget_title">Categories</h2>
			<?php wp_list_categories('show_count=1&title_li='); ?>
			
</div>

<div class="fix"></div>
		
</div>

		<?php endif; ?>

<?php 
	if(current_user_can('read_internal_only')):
		get_sidebar('internal'); 
	endif;
?>

</div>