<?php
/*
Template Name: Intranet template
*/
?>
<?php
// if(current_user_can('read_internal_only')):
?>
	<?php get_header(); ?>
    
	 <?php
		$query = new WP_Query( array('post_type'=>'intranet') );

		while( $query->have_posts() ): 
			$query->the_post()
		?>
        
			<div class="posts">
		
				<div id="section latest">
					<dl>
					<dt><a href="#"><?php the_title(); ?></a></dt>
					<dd>
							<?php 
							if ( $last_id = get_post_meta($post_ID, '_edit_last', true) ):
								$last_user = get_userdata($last_id);
								printf(__('Page last updated by %1$s on %2$s at %3$s'), wp_specialchars( $last_user->display_name ), mysql2date(get_option('date_format'), $post->post_modified), mysql2date(get_option('time_format'), $post->post_modified)); 
							else: 
								printf(__('Page last updated on %1$s at %2$s'), mysql2date(get_option('date_format'), $post->post_modified), mysql2date(get_option('time_format'), $post->post_modified));
							endif;
							?>
					</dd>
				</div>
			
			</div>
	
		<?php 
		endwhile; 
		
		else: /* not have posts */ 
		?>

				<p>Sorry, we found no content matching your criteria.</p>

		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>
	<?php get_footer(); ?>
	
<?php
// endif;

/*
$movies = new WP_Query(
	array( 		
		'post_type' => 'movie_reviews',		
		'order' => 'DESC',
		'orderby' => 'date'	
	) 
); 
<?php while ( $movies->have_posts() ) : $movies->the_post(); ?>
	<?php the_title(); ?>	
	<?php the_content(); ?>	
	... etc ...
<?php endwhile; ?>
*/
?>