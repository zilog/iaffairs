<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">



<title>

        <?php if ( is_home() ) { ?><? bloginfo('name'); ?>&nbsp;|&nbsp;<?php bloginfo('description'); ?><?php } ?>

		<?php if ( is_search() ) { ?><? bloginfo('name'); ?>&nbsp;|&nbsp;Search Results<?php } ?>

		<?php if ( is_author() ) { ?><? bloginfo('name'); ?>&nbsp;|&nbsp;Author Archives<?php } ?>

		<?php if ( is_single() ) { ?><?php wp_title(''); ?>&nbsp;|&nbsp;<? bloginfo('name'); ?><?php } ?>

		<?php if ( is_page() ) { ?><?php wp_title(''); ?>&nbsp;|&nbsp;<? bloginfo('name'); ?><?php } ?>

		<?php if ( is_category() ) { ?><?php single_cat_title(); ?>&nbsp;|&nbsp;Archive&nbsp;|&nbsp;<? bloginfo('name'); ?><?php } ?>

		<?php if ( is_month() ) { ?><?php the_time('F'); ?>&nbsp;|&nbsp;Archive&nbsp;|&nbsp;<? bloginfo('name'); ?><?php } ?>

        <?php if (function_exists('is_tag')) { if ( is_tag() ) { ?><?php bloginfo('name'); ?>&nbsp;|&nbsp;Tag Archive&nbsp;|&nbsp;<?php  single_tag_title("", true); } } ?>

</title>



	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

    <link rel="icon" type="image/png" href="<?php echo get_option('inuitypes_favicon'); ?>" /> 

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php if ( get_option('inuitypes_scripts_header') <> "" ) { echo stripslashes(get_option('inuitypes_scripts_header')); } ?>



<?php wp_head(); ?>



</head>
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://stats.rietveldacademie.nl/" : "http://stats.rietveldacademie.nl/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 10);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://stats.rietveldacademie.nl/piwik.php?idsite=10" style="border:0" alt=""/></p></noscript>
<!-- End Piwik Tag -->


<body>



<?php include(TEMPLATEPATH . '/library/functions/session.php'); ?>



	<div class="wrapper">

	

	<div class="top_menu">

	

	    <div class="fl">

	

            <ul id="pagenav" class="page-menu">

			

			    <li <?php if ( is_home() ) { ?> class="current_page_item" <?php } ?>><a href="<?php echo get_option('home'); ?>/">Home</a></li>

					            

			    <?php wp_list_pages('title_li=&depth=0&exclude=' . get_inc_pages("pag_exclude_") .'&sort_column=menu_order'); ?>
			        

		    </ul> 

	   

	    </div>



	    <div class="fr">

		    

			<span class="subscribe">

			

			<?php if (!get_option('inuitypes_admin_link')) {} else { ?><a class="admin-link" href="<?php echo get_option('home'); ?>/wp-admin/"><?php echo $admin_name; ?></a><?php } ?>

			

			<a href="<?php bloginfo('rss2_url'); ?>" title="subscribe to the Internal Affairs feed"><span class="rss-button">RSS</span></a>


			

			</span>

			

	    </div>

	

	    </div>

		

		<!-- AdSense Top Single: START -->

	

	        <?php if (get_option('inuitypes_header_adsense') <> "") { ?>

					

			    <div class="adsense-728">

		

		            <?php echo stripslashes(get_option('inuitypes_header_adsense')); ?>

		

		        </div>

												

		    <?php } ?>	

		

        <!-- AdSense Top Single: END -->

		

		<div class="page">



			<div class="content <?php if ( !get_option('inuitypes_right_sidebar') ) { echo 'content_right'; } else { echo 'content_left'; } ?>">



                <div id="header">

				

				    <?php if ( get_option('inuitypes_show_blog_title') ) { ?>

			

			            <div class="blog-title"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></div>

			

		                <div class="blog-description"><?php bloginfo('description'); ?></div>

			

			        <?php } else { ?>

				

                        <h1 class="logo"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php if ( get_option('inuitypes_logo_url') <> "" ) { echo get_option('inuitypes_logo_url').'"'; } ?>" alt="<?php bloginfo('name'); ?>" /></a></h1>



				    <?php } ?>	

					

				</div>

				