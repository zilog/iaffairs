<?php

// =============================== Ads 125x125 widget ======================================

function adsWidget()
{
$img_url[1] = get_option('inuitypes_ad_image_1');
$dest_url[1] = get_option('inuitypes_ad_url_1');
$img_url[2] = get_option('inuitypes_ad_image_2');
$dest_url[2] = get_option('inuitypes_ad_url_2');
$img_url[3] = get_option('inuitypes_ad_image_3');
$dest_url[3] = get_option('inuitypes_ad_url_3');
$img_url[4] = get_option('inuitypes_ad_image_4');
$dest_url[4] = get_option('inuitypes_ad_url_4');
$img_url[5] = get_option('inuitypes_ad_image_5');
$dest_url[5] = get_option('inuitypes_ad_url_5');
$img_url[6] = get_option('inuitypes_ad_image_6');
$dest_url[6] = get_option('inuitypes_ad_url_6');

?>

<div class="box3">

<?php if ( get_option('inuitypes_show_ads_top12') ) { ?>
       
    <div class="ads123456"> 
        
        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[1]"; ?>"><img src="<?php echo "$img_url[1]"; ?>" alt="" /></a>

        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[2]"; ?>"><img src="<?php echo "$img_url[2]"; ?>" alt="" class="last" /></a>
        
    </div>
	
	<div class="fix"></div>
                
<?php } ?>

<?php if ( get_option('inuitypes_show_ads_top34') ) { ?>
       
    <div class="ads123456"> 
        
        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[3]"; ?>"><img src="<?php echo "$img_url[3]"; ?>" alt="" /></a>

        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[4]"; ?>"><img src="<?php echo "$img_url[4]"; ?>" alt="" class="last" /></a>
        
    </div> 

    <div class="fix"></div>	

<?php } ?>

<?php if ( get_option('inuitypes_show_ads_top56') ) { ?>
       
    <div class="ads123456"> 
        
        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[5]"; ?>"><img src="<?php echo "$img_url[5]"; ?>" alt="" /></a>

        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$dest_url[6]"; ?>"><img src="<?php echo "$img_url[6]"; ?>" alt="" class="last" /></a>
        
    </div> 

    <div class="fix"></div>	

<?php } ?>

</div>
<!--/box3 -->

<?php }

register_sidebar_widget('Ads 125x125', 'adsWidget');

function adsWidgetAdmin() {

	echo '<input type="hidden" id="update_ads" name="update_ads" value="1" />';

}

register_widget_control('Ads 125x125', 'adsWidgetAdmin', 200, 200);

// =============================== Ad 250x250 widget ======================================

function adoneWidget()
{
?>

<?php if ( !get_option('inuitypes_not_200') ) { ?>

<?php 

	if ( get_option('inuitypes_home_only') ) { 
	
		if ( is_home() ) {

?>

<div class="box3">

    <div id="big_banner">
  
		<?php
                    
            // Get block code //
            $block_img = get_option('inuitypes_block_image');
            $block_url = get_option('inuitypes_block_url');
                
        ?>
                
        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$block_url"; ?>"><img src="<?php echo "$block_img"; ?>" alt="" /></a>

    </div>
    
</div>

<?php } } else { ?>

<div class="box3">

    <div id="big_banner">
      
        <?php
                    
            // Get block code //
            $block_img = get_option('inuitypes_block_image');
            $block_url = get_option('inuitypes_block_url');
                
        ?>
                
        <a <?php do_action('inuitypes_external_ad_link'); ?> href="<?php echo "$block_url"; ?>"><img src="<?php echo "$block_img"; ?>" alt="" /></a>
    
    </div>
    
</div>

<?php } } }

register_sidebar_widget('Ad 250x250', 'adoneWidget');

function adoneWidgetAdmin() {

	echo '<input type="hidden" id="update_ads" name="update_ads" value="1" />';

}

register_widget_control('Ad 250x250', 'adoneWidgetAdmin', 200, 200);


// =============================== Flickr widget ======================================

function flickrWidget()
{
	$settings = get_option("widget_flickrwidget");

	$id = $settings['id'];
	$number = $settings['number'];

?>

<div class="widget flickr">
			
        <h2 class="widget_title"><span>flick<span>r</span></span> photostream</h2>
		
		<div class="fix"></div>
            			
            <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $number; ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $id; ?>"></script>  
		
		<div class="fix"></div>
		
		</div>
		
        <div class="fix"></div>
		
</div>

<?php
}
function flickrWidgetAdmin() {

	$settings = get_option("widget_flickrwidget");

	// check if anything's been sent
	if (isset($_POST['update_flickr'])) {
		$settings['id'] = strip_tags(stripslashes($_POST['flickr_id']));
		$settings['number'] = strip_tags(stripslashes($_POST['flickr_number']));

		update_option("widget_flickrwidget",$settings);
	}

	echo '<p>
			<label for="flickr_id">Flickr ID (<a href="http://www.idgettr.com">idGettr</a>):
			<input id="flickr_id" name="flickr_id" type="text" class="widefat" value="'.$settings['id'].'" /></label></p>';
	echo '<p>
			<label for="flickr_number">Number of photos:
			<input id="flickr_number" name="flickr_number" type="text" class="widefat" value="'.$settings['number'].'" /></label></p>';
	echo '<input type="hidden" id="update_flickr" name="update_flickr" value="1" />';

}

register_sidebar_widget('Flickr Photostream', 'flickrWidget');
register_widget_control('Flickr Photostream', 'flickrWidgetAdmin', 250, 200);

?>