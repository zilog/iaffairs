<?php get_header(); ?>



<?php include(TEMPLATEPATH . '/library/functions/session.php'); ?>



    <div class="browsing-archive">

    

	    <?php if (have_posts()) : ?>

		

        		<div id="header-about">

								

            	<h2><?php echo $browsing_search_name; ?> &raquo;<?php printf(__('%s'), $s) ?>&laquo;</h2>        

				

				</div>

	</div>

	

	<div class="blog">

		

		<?php while (have_posts()) : the_post(); ?>	

            

            <div class="post">

            

			    <h2><a title="Permanent Link to <?php the_title(); ?>" href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>

				

			    <div class="date-comments">

                    

				    <p class="fl"><em><?php the_time('F j, Y'); ?></em></p>

                    

				    <p class="fr"><span class="comments"><?php comments_popup_link('meer/more', '1', '%'); ?></span></p>

                

			    </div>

				

			    <div class="fix"></div>

                

			    <p><?php echo strip_tags(get_the_excerpt(), '<a><strong>'); ?></p>

				

            </div>



		<?php endwhile; ?>

		

		<?php else : ?>

			

			<div class="browsing-archive">

    		

        		<div id="header-about">

								

            	<h2><?php echo $browsing_searchresults_name; ?> &raquo;<?php printf(__('%s'), $s) ?>&laquo;</h2>        

				

				</div>

	        </div>

		

		<?php endif; ?>

		

		<div class="fix"></div>

	

	    <div class="pagination">

			

            <?php if (function_exists('wp_pagenavi')) { ?><?php wp_pagenavi(); ?><?php } ?>

						

        </div>

					

    </div>		

		

</div>



<?php get_sidebar(); ?>

   	    

<?php get_footer(); ?>

