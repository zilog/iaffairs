<?php include(TEMPLATEPATH . '/library/functions/comments_session.php'); ?>



<?php // Do not delete these lines

	

	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))

	

		die ('Please do not load this page directly. Thanks!');



	if ( post_password_required() ) { ?>

	

		<p class="nocomments"><?php echo $comment_password_name; ?></p>

		

	<?php

	

		return;

	}



	/* This variable is for alternating comment background */

	$oddcomment = 'alt';

?>



<?php



	global $bm_comments;

	

	global $bm_trackbacks;

	

	split_comments( $comments );

	

?>



<!-- You can start editing here. -->



<?php if ( have_comments() ) : ?>



	

	<?php

	

		$trackbackcounter = count( $bm_trackbacks );

		

		$commentcounter = count( $bm_comments );

		

	?>



	<div class="commh2"><?php comments_number(''.$comment_responsesa_name.'', ''.$comment_responsesb_name.'', ''.$comment_responsesc_name.'' );?> &rarr; &#8220;<?php the_title(); ?>&#8221;</div>

		

	<div class="navigation">

	

		<div class="alignleft"><?php previous_comments_link() ?></div>

		

		<div class="alignright"><?php next_comments_link() ?></div>

		

	</div>



	<ol>

	

	<?php wp_list_comments('type=comment&callback=inuitypes_comment'); ?>

	

	</ol>

	

	<div class="navigation">

	

		<div class="alignleft"><?php previous_comments_link() ?></div>

		

		<div class="alignright"><?php next_comments_link() ?></div>

		

	</div>



	<?php if ( count( $bm_trackbacks ) > 0 ) { ?>



	<div class="commh2"><?php echo $trackbackcounter; ?> <?php echo $comment_trackbacks_name; ?></div>



	<ol class="commentlist">



	<?php foreach ($bm_trackbacks as $comment) : ?>



		<li class="<?php echo $oddcomment; ?> <?php if(function_exists("author_highlight")) author_highlight(); ?>" id="comment-<?php comment_ID() ?>">



			<cite><?php comment_author_link() ?></cite> &rarr;

			<?php if ($comment->comment_approved == 'meer/more') : ?>

			<em><?php echo $comment_moderation_name; ?></em>

			<?php endif; ?>

			<br />



			<small class="commentmetadata"><a href="#comment-<?php comment_ID() ?>" title=""><?php comment_date('F jS, Y') ?> &rarr; <?php comment_time() ?></a> <?php edit_comment_link('e','',''); ?></small>



			<?php comment_text() ?>



		</li>



	<?php /* Changes every other comment to a different class */

		if ('alt' == $oddcomment) $oddcomment = '';

		else $oddcomment = 'alt';

	?>



	<?php endforeach; /* end for each comment */ ?>



	</ol>



	<?php } ?>



 <?php else : // this is displayed if there are no comments so far ?>



	<?php if ('open' == $post->comment_status) : ?>

		<!-- If comments are open, but there are no comments. -->

		<div class="commh2"><?php echo $comment_conversation_name; ?></div>



	 <?php else : // comments are closed ?>

		<!-- If comments are closed. -->

		<p class="nocomments"><?php echo $comment_closed_name; ?></p>



	<?php endif; ?>

<?php endif; ?>





<?php if ('open' == $post->comment_status) : ?>



<div id="respond">



<div class="cancel-comment-reply">



	<small><?php cancel_comment_reply_link(); ?></small>

	

</div>



<div class="commh2"><?php comment_form_title( ''.$comment_reply_name.'', ''.$comment_reply_name.' &rarr; %s' ); ?></div>



<?php if ( get_option('comment_registration') && !$user_ID ) : ?>

<p class="alert"><?php echo $comment_mustbe_name; ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>"><?php echo $comment_loggedin_name; ?></a> <?php echo $comment_postcomment_name; ?></p>

<?php else : ?>



<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">



<?php if ( $user_ID ) : ?>



<p><?php echo $comment_loggedin_name; ?> &rarr; <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account"><?php echo $comment_logout_name; ?> &raquo;</a></p>



<?php else : ?>



<p class="commpadd"><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />

<label for="author"><small><?php echo $comment_name_name; ?> <?php if ($req) _e('*'); ?></small></label></p>



<p class="commpadd"><input type="text" name="email" id="email" value="<?php echo $comment_auth_email; ?>" size="22" tabindex="2" />

<label for="email"><small><?php echo $comment_mail_name; ?> <?php if ($req) _e('*'); ?></small></label></p>



<p class="commpadd"><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />

<label for="url"><small><?php echo $comment_website_name; ?></small></label></p>



<?php endif; ?>



<!--<p><small><strong>XHTML:</strong> You can use these tags: <?php echo allowed_tags(); ?></small></p>-->



<p class="commpadd"><textarea name="comment" id="comment" style="width:100%;" rows="10" tabindex="4"></textarea></p>



<p style="padding:10px 0px 10px 0px;"><input name="submit" type="submit" id="submit" tabindex="5" value="<?php if ( get_option('inuitypes_comment_addcomment_name') <> "" ) { ?><?php echo $comment_addcomment_name; ?><?php } else { ?>Add Comment <?php } ?>" />

<?php comment_id_fields(); ?>

</p>

<?php do_action('comment_form', $post->ID); ?>



</form>



<?php endif; // If registration required and not logged in ?>

</div>



<?php endif; // if you delete this the sky will fall on your head ?>

