<?php

/*
$pages = get_children(array(
'orderby' => 'post_date', 
'post_type' => 'news',
'post_status' => 'future',
'numberposts' => "3",
'tax_query' => array(
array(
'taxonomy' => 'news',
'field'    => 'id',
'terms'    => 51,
),
),));


$wpq = array(
         'post_type' => 'document-tutorial',
           'orderby'  => 'title',
           'order'=>ASC,
           'posts_per_page'=>-1,
           'tax_query'=>
            array(
                         'taxonomy' => 'document-category',
           		'field' => 'term_taxonomy_id',
			'terms' => array($term_tax_id),
			'include_children' => false
			)
           );


				// We want only the posts for a given taxonomy value (and exclude its children)
				  $wpq = array(
				         'post_type' => 'document-tutorial',
				           'orderby'  => 'title',
				           'order'=>ASC,
				           'posts_per_page'=>-1,
				           'tax_query'=>
				            array('relation' => 'AND',
				              array( 'taxonomy' => 'document-category',
							'field' => 'slug',
							'terms' => $category->name,
							'operator' => 'IN'
							),
				              array( 'taxonomy' => 'document-category',
							'field' => 'id',
							'terms' => $termchildren,
							'operator' => 'NOT IN'
							)
					      )
				           );

				  // Query to get the posts
				  $parentposts = new WP_Query($wpq);



					// if is taxonomy query for 'collections' taxonomy, modify query so only posts in that collection (not posts in subcollections) are shown.
					if (is_tax()) {
					 if (get_query_var('collection')) {
					  $taxonomy_term_id = $wp_query->queried_object_id;
					  $taxonomy = 'collection';
					  $unwanted_children = get_term_children($taxonomy_term_id, $taxonomy);
					  $unwanted_post_ids = get_objects_in_term($unwanted_children, $taxonomy);

					  // merge with original query to preserve pagination, etc.
					  query_posts( array_merge( array('post__not_in' => $unwanted_post_ids), $wp_query->query) );
					 }
					}
					
*/
if(current_user_can('read_internal_only')):
?>
	<?php get_header(); ?>
    
	 <?php if (have_posts()) : ?>
 
		<?php while (have_posts()) : the_post(); ?>
        
			<div class="post">
		
			    <div id="header-about">
    
		            <h2><?php the_title(); ?> <?php edit_post_link('<span class="edit-entry">Edit this entry</span>'); ?></h2>
			
				</div>
			          
	            <div class="entry">
				
				    <?php the_content(); ?>
					
	            </div>
			
				<div class="last-updated">
						
			        <?php if ( $last_id = get_post_meta($post_ID, '_edit_last', true) ) {
	            
					$last_user = get_userdata($last_id);
	            
					printf(__('Page last updated by %1$s on %2$s at %3$s'), wp_specialchars( $last_user->display_name ), mysql2date(get_option('date_format'), $post->post_modified), mysql2date(get_option('time_format'), $post->post_modified));} 
	            
					else 
				
					{printf(__('Page last updated on %1$s at %2$s'), mysql2date(get_option('date_format'), $post->post_modified), mysql2date(get_option('time_format'), $post->post_modified));}
				
					?>
							
			    </div>
                
			</div>
	
		<?php endwhile; else: ?>

				<p>Sorry, no posts matched your criteria.</p>

		<?php endif; ?>

	</div>

	<?php get_sidebar(); ?>
    	    
	<?php get_footer(); ?>
	
<?php
endif;
?>