<?php
/*
Template Name: GRAnet - dashboard
*/
?>
<!doctype html>
<!--[if lt IE 8]> <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 oldie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>GRAnet Internal Home</title>
	<meta name="description" content="Gerrit Rietveld Academie Internal Affairs">
	<meta name="author" content="derFunke">

	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	
	<link href='<?= get_template_directory_uri() ?>/res/css/intranet.css' rel='stylesheet' type='text/css'></link>

</head>
<body>

<!--
	<div id="locationbar" class="login-logout">
		<div id="logincontrol"><a href="https://launchpad.net/+login">Log out</a></div>
	</div>
-->
<?php
	// get current user details
	global $current_user;
	get_currentuserinfo();
?>
	<!-- begin: header -->
	<div id="header-container" class="header">
		<div>
			<?= get_avatar($current_user->ID, 64) ?>
			<!-- <img width="64" height="64" src="res/imgs/placeholder.png" alt="" title="" /> -->
		</div>
		<div class="headline">
			<h1>Hello <?= $current_user->user_firstname ?>,<span class="link-to-all" style="font-size:14px;"><a href="<?php echo wp_logout_url(home_url()); ?>" title="Logout">Logout</a></h1>
			<h2>Welcome to Internal Affairs</h2>
		</div>
	</div>
	<!-- end: header -->

	<!-- begin: navigation -->
	<div id="navigation">
		<?php echo wp_nav_menu( array( 'theme_location' => 'intra-nav-menu' ) ); ?>
<!--
		<ul class="menu">
			<li class="active">Home</li>
			<li><a href="#">My documents</a></li>
			<li><a href="#">People</a></li>
			<li><a href="#">Schedule</a></li>
		</ul>
-->
	</div>
	<!-- end: navigation -->

	<div id="content-container">
	<?php
	if (have_posts()):
			the_post();
			the_content();
	endif; 
	?>

		<div id="dashboard-projects">

			<div class="project wide">
				<h2>Latest news<span class="link-to-all"><a href="#">all news &#187;</a></span></h2>
				<dl>
					<dt><a href="#">Waar vind ik wat op kunst- en vormgevingsgebied? In progress!<a/></dt>
					<dd>by rentoraat on 2 July 2013</dd>
					<dt><a href="#">STAYING IN THE NETHERLANDS AFTER GRADUATION</a></dt>
					<dd>by rentoraat on 2 July 2013</dd>
					<dt><a href="#">Brief DUO – Wijzinging urennorm EU-studenten</a></dt>
					<dd>by rentoraat on 2 July 2013</dd>
					<dt><a href="#">TO APPLY – MVV VISA and RESIDENCE PERMIT</a></dt>
					<dd>by rentoraat on 2 July 2013</dd>
				</dl>
			</div>

			<div class="project wide" style="margin-bottom: 2em;">
				<h2>Recently shared documents<span class="link-to-all"><a href="#">all documents &#187;</a></span></h2>
				<table id="tab-recent-docs">
					<tr>
						<th>title</th>
						<th>filename</th>
						<th>size</th>
						<th>author</th>
					</tr>
					<tr>
						<td>Rules of engagement</td>
						<td><a href="#">gra-engagement-rules.doc</a></td>
						<td>75Kb</td>
						<td>webmaster</td>
					</tr>
					<tr class="shade">
						<td>Post-dramatic theatre and its discontents</td>
						<td><a href="#">gra-postdrama.doc</a></td>
						<td>3Mb</td>
						<td>karin</td>
					</tr>
					<tr>
						<td>Intranet Manual</td>
						<td><a href="#">gra-intranet-manual.doc</a></td>
						<td>8.5Mb</td>
						<td>karin</td>
					</tr>
					<tr class="shade">
						<td>Birds of a Feather Get Together</td>
						<td><a href="#">gra-bridos.doc</a></td>
						<td>1.2Mb</td>
						<td>webmaster</td>
					</tr>
				</table>
			</div>

			<div class="project">
				<h2>Off duty</h2>
				<dl>
					<dt><a href="#">Karien Wielenga</a></dt>
					<dd style="font-weight:bold;">department: voorkurs, untill 12 AUG, vacation</dd>
					<dt><a href="#">Steven Jongejan</a></dt>
					<dd style="font-weight:bold;">department: staff, untill 1 SEP, vacation</dd>
					<dt><a href="#">Karin Houkes</a></dt>
					<dd style="font-weight:bold;">department: student adminitratie, untill 1 SEP, vacation</dd>
				</dl>
			</div>

			<div class="project another">
				<h2>Search</h2>
				<form action="">
					<fieldset>
						<input type="text" name="s" id="s" />
						<input type="button" name="submit" id="submit" value="Search intranet &#187;"></input>
					</fieldset>
					<fieldset>
						<label>Include:</label>
						<input type="checkbox" id="web" checked="checked" name="web" value="yes">website</input>
						<input type="checkbox" id="documents" checked="checked" name="documents" value="yes">documents</input>
						<input type="checkbox" id="people" checked="checked" name="people" value="yes" >people</input>
					</fieldset>
				</form>
			</div>

		<div>
	</div>

	<div class="clearfix"></div>

	<!-- begin: footer -->
	<div id="footer" class="footer">
		<div class="colophon">
			© 2013-2014
			<a href="http://rietveldacademie.nl">Gerrit Rietveld Academie</a>
			&nbsp;•&nbsp;
			
			<?php echo wp_nav_menu( array( 
																'theme_location' => 'intra-footer-menu', 
																'container' => '',
																'container_class' => '',
																'before'          => '',
																'after'           => '',
																'link_before'     => '',
																'link_after'      => '',
																'items_wrap'      => '<a href="%1$s">%3$s</a>'
														));
			?>
			
			<!-- <a href="#">Terms of use</a>
			&nbsp;•&nbsp;
			<a href="#">Contact</a>

			&nbsp;•&nbsp;
			<a href="#">Blog</a>

			&nbsp;•&nbsp;
			<a href="#">System status</a>

			&nbsp;•&nbsp;
			<a href="#">Developers</a> -->
		</div>
	</div>
	<!-- end: footer -->

</body>
</html>


<!--
CSS CODE
#navcontainer ul
{
margin: 0;
padding: 0;
list-style-type: none;
text-align: center;
}

#navcontainer ul li { display: inline; }

#navcontainer ul li a
{
text-decoration: none;
padding: .2em 1em;
color: #fff;
background-color: #036;
}

#navcontainer ul li a:hover
{
color: #fff;
background-color: #369;
}
-->